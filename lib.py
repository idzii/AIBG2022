import json
import random


class GameState:
    playerIdx = "" 
    bossCoords = [(0,0), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1)]
    def __init__(self, res):
       # self.playerIdx = "1"  # hard kodiran indeks
       # self.playerIdx = res['playerIdx']
        
       # print('Consturctor()' + GameState.playerIdx)

        pom = json.loads(res["gameState"])
       # print(type(pom["map"]))
        self.map = {(tile['q'], tile['r']) : tile for row in pom["map"]["tiles"] for tile in row} #pom["map"]
        self.player = pom["player" + GameState.playerIdx]
        self.enemies = [pom["player" + str(i)] for i in range(1,5) if str(i)!=GameState.playerIdx]
        self.asteroids = list(filter(lambda x: x["entity"]["type"]=="ASTEROID" , self.map.values()))
        self.blackHoles = list(filter(lambda x: x["entity"]["type"]=="BLACKHOLE" , self.map.values()))
        self.health = list(filter(lambda x: x["entity"]["type"]=="HEALTH" , self.map.values()))[0]
        self.xp = list(filter(lambda x: x["entity"]["type"]=="EXPERIENCE" , self.map.values()))[0]
        self.wormholes = list(filter(lambda x: x["entity"]["type"]=="WORMHOLE" , self.map.values()))

        self.enemies = list(filter(lambda e: e is not None, self.enemies))
        print(self.xp)

    #    print(res)
    #    print("------------------------------------")
    def isLegalMove(self, q, r):
        if q > 14 or q < -14 or r > 14 or r < -14:
            return False
        if abs(q+r) > 14:
            return False
        
        if self.map[(q,r)]["tileType"] == "FULL":
            return False
        for enemy in self.enemies:
            if enemy["q"] == q and  enemy["r"] == r:
                return False 

        if (q,r) in self.bossCoords:
            return False

        ## ne idi u black hole
        if (q, r) in (map(lambda b: (b['q'], b['r']), self.blackHoles)):
            return False

        return True


    def playerMove(self):
        q, r = self.player['q'], self.player['r']
        
        listOfMoves = [(q1, r1) for q1, r1 in [(q, r-1), (q+1, r-1),(q+1, r), (q, r+1), (q-1, r+1), (q-1, r)] if self.isLegalMove(q1,r1)]

        xQ, xR = self.xp['q'], self.xp['r']
        hQ, hR = self.health['q'], self.health['r']
        print(xQ, xR)
     
        move_towards = ""

        if distance((q, r), (xQ, xR)) < 5:
            listOfMoves.sort(key=lambda x: distance(x, (xQ,xR)))  # koliko je daleko xp od potencijalnih poteza
            move_towards = "XP"
        elif self.player['health'] <= 601 and distance((q, r), (hQ, hR)) < 6:
            listOfMoves.sort(key=lambda x: distance(x, (hQ,hR)))  # koliko je daleko health od potencijalnih poteza
            move_towards = "HP"
        else:
            listOfMoves.sort(key=lambda x: distance(x, (0,0)))  # koliko je daleko boss od potencijalnih poteza
            move_towards = "BOSS"
        return listOfMoves[0], move_towards

    def isLegalAttack(self, q, r):
        if q > 14 or q < -14 or r > 14 or r < -14:
            return False
        if abs(q+r) > 14:
            return False
        
        playerQ, playerR = self.player['q'], self.player['r']
        
        if distance([q,r], [playerQ, playerR] > 3):
            return False
        
        return True 

    def playerAttack(self):
        playerQ, playerR = self.player['q'], self.player['r']
        targets = []
        for asteroid in self.asteroids:
            aQ, aR = asteroid['q'], asteroid['r']
            priority = 20 + (350-asteroid['entity']['health']) / 350 

            if distance([aQ, aR], [playerQ,playerR]) < 2: # udara samo asteroide udaljene 1
                targets.append((aQ, aR, priority))

        for enemy in self.enemies:
            eQ, eR = enemy['q'], enemy['r']
            priority = 30
            ## todo

            if distance((eQ, eR), (playerQ, playerR)) < 4:
                targets.append((eQ, eR, priority))

        for bQ, bR  in self.bossCoords:
            priority = 50

            if distance((bQ, bR), (playerQ, playerR)) < 4:
                targets.append((bQ, bR, priority))


        if len(targets) == 0:
            return None

       # selected = targets[random.randrange(0, len(targets))]
        targets.sort(key=lambda x: x[2], reverse=True)
        return targets[0]

    def play(self):
        attack = self.playerAttack()
        move, info = self.playerMove()
        if attack is not None and info != "HP":
            return "attack," + str(attack[0]) + "," + str(attack[1])
        else:
            return "move," + str(move[0]) + "," + str(move[1])

def distance(a, b):
    return (abs(a[0] - b[0]) 
          + abs(a[0] + a[1] - b[0] - b[1])
          + abs(a[1] - b[1])) / 2